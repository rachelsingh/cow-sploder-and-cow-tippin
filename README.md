# Cow Splode and Cow Tippin

**Year:** 2008

**Tools:** VB5 (Cow Splode), C++/Allegro4 (Cow Tippin)

## Screenshots

![Screenshot of game](screenshot-cs.png)

Cow Splode

![Screenshot of game](screenshot-ct.png)

Cow Tippin
